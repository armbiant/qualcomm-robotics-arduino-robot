#include <Arduino.h>
#include <Encoder.h>
#include <TimerOne.h>
#include <PID_v1.h>

#define ENABLE A2 // Enable Pin for the Motors

#define PWM_L_FWD 3 // PWM (analog) Pin for the Left Motor; Turn Forward
#define PWM_L_BWD 5 // PWM (analog) Pin for the Left Motor; Turn Backwards

String mySt = "";
boolean stringComplete = false; // whether the string is complete
boolean motor_start = false;

volatile int last_encoder_left = 0;
volatile int encoderL = 0;
volatile int delta_l = 0;
volatile double rps = 0;
double e_speed = 0;            //error of speed = set_speed - pv_speed
double e_speed_pre = 0;        //last error of speed
double e_speed_sum = 0;        //sum error of speed

double pwm_pulse = 0; //this value is 0~255 AAA
double set_speed = 0;  // AAA
double pv_speed = 0;   // AAA

double kp;
double ki;
double kd;

//Specify the links and initial tuning parameters
PID myPID(&pv_speed, &pwm_pulse, &set_speed, kp, ki, kp, P_ON_M, DIRECT);

const int pin_a = 20; //A channel for encoder of left motor
const int pin_b = 21; //B channel for encoder of left motor

Encoder encL(pin_a, pin_b);

void doLeftPID()
{
    //PID program
    if (motor_start)
    {
        e_speed = set_speed - pv_speed;
        pwm_pulse = e_speed * kp + e_speed_sum * ki + (e_speed - e_speed_pre) * kd;
        e_speed_pre = e_speed;  //save last (previous) error
        e_speed_sum += e_speed; //sum of error
        if (e_speed_sum > 4000)
            e_speed_sum = 4000;
        if (e_speed_sum < -4000)
            e_speed_sum = -4000;
    }
    else
    {
        e_speed = 0;
        e_speed_pre = 0;
        e_speed_sum = 0;
        pwm_pulse = 0;
    }
}

void timerCallback() // interrupt service routine - tick every 0.1sec
{
    encoderL = encL.read();

    delta_l = abs(abs(encoderL) - abs(last_encoder_left));
    //Serial.print("delta: ");
    //Serial.print(delta_l);

    last_encoder_left = encoderL;

    rps = (delta_l / 1296.0) / 0.01;
    //Serial.print(" rps: ");
    //Serial.print(rps);

    pv_speed = 60.0 * rps; //calculate motor speed, unit is rpm
    //print out speed
    if (Serial.available() <= 0)
    {
        Serial.print("speed");
        Serial.println(pv_speed); //Print speed (rpm) value to Visual Studio
    }

    //doLeftPID();
    myPID.Compute();

    //update new speed
    if (pwm_pulse < 255 && pwm_pulse > 0)
    {
        analogWrite(PWM_L_FWD, pwm_pulse); //set motor speed
    }
    else
    {
        if (pwm_pulse > 255)
        {
            analogWrite(PWM_L_FWD, 255);
        }
        else
        {
            analogWrite(PWM_L_FWD, 0);
        }
    }

    //Serial.print(" rpm: ");
    //Serial.println(pv_speed); //Print speed (rpm) value to Visual Studio
}

void setup()
{
    // start serial port at 9600 bps:
    Serial.begin(115200);
    //--------------------------timer setup
    Timer1.initialize(10000);
    noInterrupts(); // disable all interrupts
    pinMode(ENABLE, OUTPUT);
    pinMode(PWM_L_BWD, OUTPUT);
    pinMode(PWM_L_FWD, OUTPUT);
    pinMode(LED_BUILTIN, OUTPUT);

    int myEraser = 7;    // this is 111 in binary and is used as an eraser
    TCCR3B &= ~myEraser; // this operation (AND plus NOT),  set the three bits in TCCR2B to 0
    TCCR4B &= ~myEraser; // this operation (AND plus NOT),  set the three bits in TCCR2B to 0

    int myPrescaler = 1;   // this could be a number in [1 , 6]. In this case, 3 corresponds in binary to 011.
    TCCR3B |= myPrescaler; //this operation (OR), replaces the last three bits in TCCR2B with our new value 011
    TCCR4B |= myPrescaler; //this operation (OR), replaces the last three bits in TCCR2B with our new value 011
    interrupts();          // enable all interrupts
    Timer1.attachInterrupt(timerCallback);
    //--------------------------timer setup

    while (!Serial)
    {
        ; // wait for serial port to connect. Needed for native USB port only
    }

    digitalWrite(ENABLE, HIGH);
    analogWrite(PWM_L_BWD, 100);
    delay(100);
    analogWrite(PWM_L_BWD, 0);
    delay(100);
    analogWrite(PWM_L_FWD, 100);
    delay(100);
    analogWrite(PWM_L_FWD, 0);
    digitalWrite(ENABLE, LOW);

    myPID.SetMode(AUTOMATIC);
    myPID.SetOutputLimits(0, 255);
    myPID.SetSampleTime(10);
}

void loop()
{
    if (stringComplete)
    {
        //receive command from Visual Studio
        if (mySt.substring(0, 8) == "vs_start")
        {
            digitalWrite(ENABLE, HIGH);
            digitalWrite(LED_BUILTIN, HIGH);
            motor_start = true;
        } else if (mySt.substring(0, 7) == "vs_stop")
        {
            digitalWrite(ENABLE, LOW);
            analogWrite(PWM_L_FWD, 0);
            analogWrite(PWM_L_BWD, 0);
            digitalWrite(LED_BUILTIN, LOW);
            motor_start = false;
        } else if (mySt.substring(0, 12) == "vs_set_speed")
        {
            set_speed = mySt.substring(12, mySt.length()).toFloat(); //get string after set_speed
        } else if (mySt.substring(0, 5) == "vs_kp")
        {
            kp = mySt.substring(5, mySt.length()).toFloat(); //get string after vs_kp
            myPID.SetTunings(kp, ki, kd);
        } else if (mySt.substring(0, 5) == "vs_ki")
        {
            ki = mySt.substring(5, mySt.length()).toFloat(); //get string after vs_ki
            myPID.SetTunings(kp, ki, kd);
        } else if (mySt.substring(0, 5) == "vs_kd")
        {
            kd = mySt.substring(5, mySt.length()).toFloat(); //get string after vs_kd
            myPID.SetTunings(kp, ki, kd);
        }
        // clear the string when COM receiving is completed

        mySt = ""; //note: in code below, mySt will not become blank, mySt is blank until '\n' is received
        stringComplete = false;
    }
}

void serialEvent()
{
    while (Serial.available())
    {
        // get the new byte:
        char inChar = (char)Serial.read();
        // add it to the inputString:
        if (inChar != '\n' && inChar != '\r')
        {
            mySt += inChar;
        }
        // if the incoming character is a newline, set a flag
        // so the main loop can do something about it:
        if (inChar == '\n')
        {
            stringComplete = true;
        }
    }
}
