#include <Arduino.h>
#include <PID_v1.h>
#include <PID_AutoTune_v0.h>

#include <Encoder.h>
#include <TimerOne.h>

byte ATuneModeRemember = 2;
double setpoint = 70;
double kp = 0, ki = 0, kd = 0.0;

double aTuneStep = 5, aTuneNoise = 5, aTuneStartValue = 80;

unsigned int aTuneLookBack = 30;

bool tuning = true;
unsigned long serialTime;

#define ENABLE A2 // Enable Pin for the Motors

#define PWM_L_FWD 3 // PWM (analog) Pin for the Left Motor; Turn Forward
#define PWM_L_BWD 5 // PWM (analog) Pin for the Left Motor; Turn Backwards

volatile int last_encoder_left = 0;
volatile int encoderL = 0;
volatile int delta_l = 0;
volatile double rps = 0;
double pv_speed = 0;  // AAA
double pwm_pulse = 0; //this value is 0~255 AAA

const int pin_a = 20; //A channel for encoder of left motor
const int pin_b = 21; //B channel for encoder of left motor

Encoder encL(pin_a, pin_b);

PID myPID(&pv_speed, &pwm_pulse, &setpoint, kp, ki, kd, DIRECT);
PID_ATune aTune(&pv_speed, &pwm_pulse);

void timerCallback() // interrupt service routine - tick every 0.1sec
{
  encoderL = encL.read();
  delta_l = abs(abs(encoderL) - abs(last_encoder_left));
  last_encoder_left = encoderL;
  rps = (delta_l / 1296.0) / 0.01;
  pv_speed = 60.0 * rps; //calculate motor speed, unit is rpm
}

void AutoTuneHelper(boolean start)
{
  if (start)
    ATuneModeRemember = myPID.GetMode();
  else
    myPID.SetMode(ATuneModeRemember);
}

void changeAutoTune()
{
  if (!tuning)
  {
    //Set the pwm_pulse to the desired starting frequency.
    pwm_pulse = aTuneStartValue;
    aTune.SetNoiseBand(aTuneNoise);
    aTune.SetOutputStep(aTuneStep);
    aTune.SetLookbackSec((int)aTuneLookBack);
    AutoTuneHelper(true);
    tuning = true;
  }
  else
  { //cancel autotune
    aTune.Cancel();
    tuning = false;
    AutoTuneHelper(false);
  }
}

void SerialReceive()
{
  if (Serial.available())
  {
    char b = Serial.read();
    Serial.flush();
    if ((b == '1' && !tuning) || (b != '1' && tuning))
      changeAutoTune();
  }
}

void SerialSend()
{
  Serial.print("setpoint: ");
  Serial.print(setpoint);
  Serial.print(" ");
  Serial.print("pv_speed: ");
  Serial.print(pv_speed);
  Serial.print(" ");
  Serial.print("pwm_pulse: ");
  Serial.print(pwm_pulse);
  Serial.print(" ");
  if (tuning)
  {
    Serial.println("tuning mode");
  }
  else
  {
    Serial.print("kp: ");
    Serial.print(myPID.GetKp());
    Serial.print(" ");
    Serial.print("ki: ");
    Serial.print(myPID.GetKi());
    Serial.print(" ");
    Serial.print("kd: ");
    Serial.print(myPID.GetKd());
    Serial.println();
  }
}

void setup()
{
  //Setup the pid
  myPID.SetMode(AUTOMATIC);

  if (tuning)
  {
    tuning = false;
    changeAutoTune();
    tuning = true;
  }

  serialTime = 0;
  Serial.begin(9600);

  Timer1.initialize(10000);
  noInterrupts(); // disable all interrupts
  pinMode(ENABLE, OUTPUT);
  pinMode(PWM_L_BWD, OUTPUT);
  pinMode(PWM_L_FWD, OUTPUT);
  pinMode(LED_BUILTIN, OUTPUT);

  int myEraser = 7;    // this is 111 in binary and is used as an eraser
  TCCR3B &= ~myEraser; // this operation (AND plus NOT),  set the three bits in TCCR2B to 0
  TCCR4B &= ~myEraser; // this operation (AND plus NOT),  set the three bits in TCCR2B to 0

  int myPrescaler = 1;   // this could be a number in [1 , 6]. In this case, 3 corresponds in binary to 011.
  TCCR3B |= myPrescaler; //this operation (OR), replaces the last three bits in TCCR2B with our new value 011
  TCCR4B |= myPrescaler; //this operation (OR), replaces the last three bits in TCCR2B with our new value 011
  interrupts();          // enable all interrupts
  Timer1.attachInterrupt(timerCallback);
  digitalWrite(ENABLE, HIGH);
}

void loop()
{
  if (tuning)
  {
    byte val = (aTune.Runtime());
    if (val != 0)
    {
      tuning = false;
    }
    if (!tuning)
    { //we're done, set the tuning parameters
      kp = aTune.GetKp();
      ki = aTune.GetKi();
      kd = aTune.GetKd();
      myPID.SetTunings(kp, ki, kd);
      AutoTuneHelper(false);
    }
  }
  else
    myPID.Compute();

  analogWrite(PWM_L_FWD, pwm_pulse);

  //send-receive with processing if it's time
  if (millis() > serialTime)
  {
    SerialReceive();
    SerialSend();
    serialTime += 100;
  }
}